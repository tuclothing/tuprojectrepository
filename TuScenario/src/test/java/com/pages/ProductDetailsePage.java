package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;


public class ProductDetailsePage extends BaseClass {		
		public void verifyproductDetailsepage() {
		Assert.assertEquals("Womens Mid Denim Slim Jean | Tu clothing",driver.getTitle());
	} 
		public void verifyEnteringProductDetailsePage () {

			WebDriverWait wait =new WebDriverWait(driver,10);
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#select-size"))).click();		
			driver.findElement(By.cssSelector("#select-size")).sendKeys("18R");		
			driver.findElement(By.cssSelector("#productVariantQty")).click();
			WebDriverWait wait1 =new WebDriverWait(driver,10);
			wait1.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#AddToCart"))).click();	
			}
			
			public void verifyClickonBasketlink() {
			driver.get("https://tuclothing.sainsburys.co.uk/");
			driver.findElements(By.cssSelector("[href='/cart']")).get(0).click();
			driver.findElements(By.cssSelector("[href='/cart']")).get(1).click();
			}

}


