Feature: Search

Background:
Given i am in homepage


Scenario Outline: search with valid data

When I search with  keyword "<searchKeyword>"
Then Should see the valid search results

Examples:
|searchKeyword|
|jeans|
|Men|
|$�*Shirts|
|Shirts,jeans|
|138138871|
|gift for 8years old |
|Fatface|
|Skinny Jeans|
|Black jeans|
|NeWarrivals|