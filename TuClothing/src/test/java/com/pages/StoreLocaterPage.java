package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

public class StoreLocaterPage extends BaseClass {
	public static WebDriverWait wait;
	public static By storeLocaterPostcode = By.cssSelector("input[name='q']");
	public void  verifystorelocaterpage() {	
	  Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}
	public void Verifysearchstorelocaterpostcode() {
	 WebDriverWait wait =new WebDriverWait(driver,10);
	 wait.until(ExpectedConditions.elementToBeClickable(storeLocaterPostcode)).click();		
	 driver.findElement(storeLocaterPostcode).sendKeys("Rg14 5Lf");
   
}
	public void storelocaterpostcode() {
	 Assert.assertEquals("Store locator search results: rg14 2fe | Tu clothing",driver.getTitle());
		
}
	public void verifytownwithstorelocater() {
		 WebDriverWait wait =new WebDriverWait(driver,10);
		 wait.until(ExpectedConditions.elementToBeClickable(storeLocaterPostcode)).click();		
		 driver.findElement(storeLocaterPostcode).sendKeys("Newbury");
	   
	}
}	