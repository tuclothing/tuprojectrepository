package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GuestCheckoutCCPage extends BaseClass {
	public void verifyWithGuestEmail() {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#guest_email"))).click();			
		driver.findElement(By.cssSelector("#guest_email")).sendKeys("sss@gmail.com");
	}
	public void verifyGuestCheckoutButton() {
		driver.findElements(By.cssSelector(".submit-container")).get(1).click();
	}
	public void verifyDeliveryOptionPage() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/delivery-options/choose",driver.getCurrentUrl());
	}
	public void verifyClickAndCollectionRadioButton() {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("label[for='CLICK_AND_COLLECT']"))).click();
	
	}
	public void verifyClickOnContinueButton() {		
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ln-c-button.ln-c-button--primary"))).click();
	}
	
	public void verifyLookUpPostcode() {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#lookup"))).click();		
		driver.findElement(By.cssSelector("#lookup")).sendKeys("Rg14 5lf");
	}
	public void verifyClickOnLookupButton() {
		driver.findElement(By.cssSelector(".LocationLookup-module__inputButtons--29DUf")).click();
	}
	
	

	}
