package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;



public class CheckoutDetailsePage  extends BaseClass {
	
	public void verifyCheckoutPage() {
	 Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/checkout", driver.getCurrentUrl());
		
	}
	public void  verifycheckoutwithexistingcustomer() {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#j_username"))).click();				
		driver.findElement(By.cssSelector("#j_username")).sendKeys("sam@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).click();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("abc1234");
	}
	
	public void verifyCheckoutButton() {
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.js-login-button")).click();
	}
	public void verifyWithGuestEmail() {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#guest_email"))).click();			
		driver.findElement(By.cssSelector("#guest_email")).sendKeys("sss@gmail.com");
	
	}
	public void verifyGuestCheckoutButton() {
		driver.findElements(By.cssSelector(".submit-container")).get(1).click();
	}
	
	
	

	
	 
	}
	