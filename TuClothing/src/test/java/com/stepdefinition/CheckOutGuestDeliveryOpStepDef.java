package com.stepdefinition;



import com.runner.BaseClass;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CheckOutGuestDeliveryOpStepDef extends BaseClass{
	
	
	
	@Then("^it should navigate Delivery option page$")
	public void it_should_navigate_Delivery_option_page() throws Throwable {
		checkoutguestdeliveryoptionpage.verifyDeliveryOptionPage() ;
}
	@When("^user click on the radio button click &collect$")
	public void user_click_on_the_radio_button_click_collect() throws Throwable {
		checkoutguestdeliveryoptionpage.verifyClickAndCollectionRadioButton();
	}
	@When("^user click on continue button$")
	public void user_click_on_continue_button() throws Throwable {
		checkoutguestdeliveryoptionpage.verifyClickOnContinueButton();
		
		}
	@When("^user enter postcode$")
	public void user_enter_postcode() throws Throwable {
		checkoutguestdeliveryoptionpage.verifyLookUpPostcode();
	}

	@When("^user click on look up button$")
	public void user_click_on_look_up_button() throws Throwable {
		checkoutguestdeliveryoptionpage.verifyClickOnLookupButton();
	}

	@Then("^it should display all nearest shops according to the postcode$")
	public void it_should_display_all_nearest_shops_according_to_the_postcode() throws Throwable {
		findccstorepage.verifyNearestCCStore();		

	
	}
	}

