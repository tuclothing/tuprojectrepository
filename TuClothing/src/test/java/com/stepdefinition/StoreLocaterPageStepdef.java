package com.stepdefinition;


import com.runner.BaseClass;

import cucumber.api.java.en.Given;

import cucumber.api.java.en.When;

public class StoreLocaterPageStepdef extends BaseClass{
	@Given("^user is in store locater page$")
	public void user_is_in_store_locater_page() throws Throwable {
		storelocaterpage.verifystorelocaterpage();
	}

	@When("^user type postcode on the search box$")
	public void user_type_postcode_on_the_search_box() throws Throwable {
		storelocaterpage.Verifysearchstorelocaterpostcode();  
	}

	/*@Then("^user could enter the postcode$")
	public void user_could_enter_the_postcode() throws Throwable {
		storelocaterpage.Verifysearchstorelocaterpostcode();
	}*/
	@When("^user type town on the search box$")
	public void user_type_town_on_the_search_box() throws Throwable {
		storelocaterpage.verifytownwithstorelocater();
	   
	}

	/*@Then("^it should show all  stores according the location$")
	public void it_should_show_all_stores_according_the_location() throws Throwable {
		storelocaterpage.verifyslocaterentertown();
	}*/


}
