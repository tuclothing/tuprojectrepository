Feature: storelocater

Scenario: Verify Store locater icon
Given user is in Tu home page
When user click on the Store locater icon on the top of the home page
Then it should show store locater page

Scenario: Verify search a Store based on postcode
Given user is in Tu home page
When user click on the Store locater icon on the top of the home page
And type postcode on the search box
Then it should show all  stores nearby postcode

Scenario: Verify  search a store based on a Location name
Given user is in Tu home page
When user click on the Store locater icon on the top of the home page
And type location on the search box
Then it should show all  stores according the location

Scenario: Verify the checkbox "Show me Click & Collect stores only"
Given user is in Tu home page
When user click on the Store locater icon on the top of the home page
And user type valid postcode on the search box
And click on click &collect 
Then it should show only click &collect shop near by postcode 

Scenario: Verify Store locater icon with missing postcode
Given user is in Tu home page
When user click on the Store locater icon on the top of the home page
And user search without entering post code
Then it should display error message

Scenario: Verify Store locater icon with other country name(outside UK) 
Given user is in Tu home page
When user click on the Store locater icon on the top of the home page
And user search with other country name (India)
Then it should display error message � No store found

