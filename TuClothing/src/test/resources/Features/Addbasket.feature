Feature: Addbasket 
@test
Scenario:  Verify  Product Image on Product details page
Given user is in home page
When user search for valid product name
And user selected one product from search results 
Then it should show product details page
@test
Scenario: verify Add to basket
Given user is in product detailse page
When user select the size & quantity.click on addtobasket button
And click on add to basket
And click on basket link
Then it should show the products in basket page

Scenario: verify update basket
Given user is in view basket page
When user update the size 
And click on update button
Then it should update the quantity in basket

Scenario: verify Add to basket without select size & quantity
Given user is in home page
When user search for valid product name
And click on the search button
And user selected one product from search results 
And it should show product details page
And user is in product detailse page
And click on add to basket
Then user shouldn't add to basket
