package com.stepdefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FindStorePageStepDef extends BaseClass {
	@When("^user select one near shop$")
	public void user_select_one_near_shop() throws Throwable {
		findccstorepage.verifySelectNearShop();
	
	}

	@When("^user click on proceed to summary$")
	public void user_click_on_proceed_to_summary() throws Throwable {
		findccstorepage.verifyProceedSummery();
	}

	@Then("^it should navigate to Review your order and choose how to pay page$")
	public void it_should_navigate_to_Review_your_order_and_choose_how_to_pay_page() throws Throwable {
		findccstorepage.verifyHowToPayPage();
	}
	
	
	}


