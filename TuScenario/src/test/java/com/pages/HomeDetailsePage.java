package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;


//public static By SearchTextBox = By.cssSelector("#search");	
//public static By SearchButton  = By .cssSelector(".button.searchButton");



public class HomeDetailsePage extends BaseClass {
  public void verifyHomePagesearch() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/",driver.getCurrentUrl());	
		}
	
	public void verifySearchAproduct(String searchKeyWord) {
		
			WebDriverWait wait =new WebDriverWait(driver,10);		
			wait.until(ExpectedConditions.elementToBeClickable( By.cssSelector("#search"))).clear(); 
			driver.findElement(By.cssSelector("#search")).sendKeys(searchKeyWord);
			driver.findElement(By.cssSelector(".button.searchButton")).click();
	}
		

	}
