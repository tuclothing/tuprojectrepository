package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

public class RegisterAndLoginPage extends BaseClass {
	public static WebDriverWait wait;
	public static By RegisterBox = By.cssSelector(".ln-c-button.ln-c-button--primary.regToggle");
	public static By EmailAddress= By.cssSelector("#register_email");
	public static By SelectTitle = By.cssSelector("#register_title");
	public static By FirstName   =   By.cssSelector("#register_firstName");
	public static By surName     = By.cssSelector("[name=lastName]");
	public static By PassWord    =  By.cssSelector("#password");
	public static By ConfirmPassWord   =   By.cssSelector("#register_checkPwd");
	public static By  SubmitRegister   =   By.cssSelector("#submit-register");
	public void verifyTheLoginAndRegisterpage() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login",driver.getCurrentUrl());
	}
	public void verifyTheRegisterButton() {
		driver.findElement(RegisterBox).click();
	}
	
	public void verifyenterregistrationform() {
		driver.findElement( EmailAddress).click();
		driver.findElement( EmailAddress).sendKeys("Sam@gmail.com");
		driver.findElement(SelectTitle).click();		
		driver.findElement(SelectTitle).sendKeys("MR");
		driver.findElement(FirstName).click();
		driver.findElement(FirstName).sendKeys("Sam");
		driver.findElement(surName).click();
		driver.findElement(surName).sendKeys("Tv");
		driver.findElement(PassWord).click();
		driver.findElement(PassWord).sendKeys("abc1234");
		driver.findElement(ConfirmPassWord ).click();
		driver.findElement(ConfirmPassWord ).sendKeys("abc1234");
		driver.findElement( SubmitRegister).click();		
	}	
	public void verifyregistercompletesuccessfull() {
	 Assert.assertEquals("https://tuclothing.sainsburys.co.uk/register/success",driver.getCurrentUrl()); 

	}
	}
				


