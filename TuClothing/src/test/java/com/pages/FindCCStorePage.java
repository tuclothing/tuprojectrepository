package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

public class FindCCStorePage extends BaseClass {
	public void verifyNearestCCStore() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/pickup-location/findStore",driver.getCurrentUrl());
	}
	public void verifySelectNearShop() {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".StoreCard-module__selectStoreButton--3CFno"))).click();	
	}
	public void verifyProceedSummery() {
		driver.findElement(By.cssSelector(".proceed-to-summary.ln-c-button.ln-c-button--primary")).click();
	}
	public void verifyHowToPayPage() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/order-summary/view",driver.getCurrentUrl());
	}
	}


