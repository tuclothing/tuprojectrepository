package com.stepdefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class searchDetailseStepDef extends BaseClass{
	

	@When("^I search for keyword \"([^\"]*)\"$")
	public void i_search_for_keyword(String searchKeyWord) throws Throwable {
		homedetailsepage.verifySearchAproduct(searchKeyWord);
	}
	@When("^I selected one product from search results page$")
	public void i_selected_one_product_from_search_results_page() throws Throwable {
		searchresultspage.verifyselectproductimage();
	
	}
	@Then("^it should show product details page$")
	public void it_should_show_product_details_page() throws Throwable {
		productdetailsepage.verifyproductDetailsepage();
	}


			

	



}
