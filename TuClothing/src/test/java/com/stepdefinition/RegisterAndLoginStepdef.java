package com.stepdefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RegisterAndLoginStepdef extends BaseClass {	
	
	
	@When("^user fill the registration form$")
	public void user_fill_the_registration_form() throws Throwable {
		registerAndLogingPage.verifyenterregistrationform();	
			}
	@When("^click on register button$")
	public void click_on_register_button() throws Throwable {
		registerAndLogingPage.verifyTheRegisterButton();
	}

	@Then("^it should show registration complete successfully$")
	public void it_should_show_registration_complete_successfully() throws Throwable {	   
		 
		registerAndLogingPage.verifyregistercompletesuccessfull();
	
	}

}