package com.runner;

import org.openqa.selenium.WebDriver;

import com.pages.BasketDetailsePage;
import com.pages.CheckoutDetailsePage;
import com.pages.CheckoutGuestDeliveryOptionPage;
import com.pages.FindCCStorePage;
import com.pages.HomePage;
import com.pages.OrderSummaryPage;
import com.pages.ProductDetailPage;
import com.pages.RegisterAndLoginPage;
import com.pages.SearchResultsPage;
import com.pages.StoreLocaterPage;

public class  BaseClass{
	
	public static WebDriver driver ;
	public static HomePage homepage =new HomePage() ;
	public static SearchResultsPage searchresultspage = new SearchResultsPage() ;

	public static ProductDetailPage productDetailPage = new ProductDetailPage() ;
	public static BasketDetailsePage basketdetailsepage = new BasketDetailsePage() ;
	public static CheckoutDetailsePage checkoutdetailsepage = new CheckoutDetailsePage () ;
	public static  StoreLocaterPage  storelocaterpage = new  StoreLocaterPage () ;
	public static RegisterAndLoginPage registerAndLogingPage = new  RegisterAndLoginPage () ;
	public static CheckoutGuestDeliveryOptionPage checkoutguestdeliveryoptionpage = new CheckoutGuestDeliveryOptionPage() ;
	public static FindCCStorePage findccstorepage = new FindCCStorePage () ;
	public static OrderSummaryPage ordersummerypage = new  OrderSummaryPage() ;
}	


