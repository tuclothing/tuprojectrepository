Feature: guestcheckoutCC
Scenario:  Verify  Product Image on Product details page
Given user is in home page
When user search for valid product name
And user selected one product from search results 
Then it should show product details page

Scenario: verify Add to basket
Given user is in product detailse page
When user select the size & quantity.click on addtobasket button
And click on add to basket
And click on basket link
Then it should show the products in basket page

Scenario: verify update basket
Given user is in view basket page
When user update the size 
And click on update button
Then it should update the quantity in basket
Scenario: verify proceed checkout button
Given user is in view basket page
When user click on proceed button
Then it should navigate to checkout detailse page

Scenario: verify checkout with guest customer
Given user is in checkout detailse page
When user enter valid email address
And user click on guest checkout button
Then it should navigate Delivery option page

Scenario: verify checkout guest with click&collect
Given  user click on the radio button click &collect
When user click on continue button
Then it should navigate click&collect Lookup page

Scenario: verify checkout guest with find your nearest click&collect
Given user enter postcode 
When user click on look up button 
Then it should display all nearest shops according to the postcode

Scenario: verify checkout guest with find your nearest click&collect shop
Given  user select one near shop 
When user click on proceed to summary 
Then it should navigate to Review your order and choose how to pay page

Scenario: verify checkout guest with payment summary
Given user is in Review your order and choose how to pay page
When user click on proceed to payment
Then it should navigate to summary&order total page