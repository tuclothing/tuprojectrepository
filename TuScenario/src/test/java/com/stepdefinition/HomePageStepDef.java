package com.stepdefinition;



import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStepDef extends BaseClass{


	
	
		@Given("^i am in homepage$")
		public void i_am_in_homepage() throws Throwable {
			homedetailsepage.verifyHomePagesearch();	
		}

		
		@When("^I search with  keyword \"([^\"]*)\"$")
		public void i_search_with_keyword(String searchKeyWord) throws Throwable {
			homedetailsepage.verifySearchAproduct(searchKeyWord);
		}
		@Then("^Should see the valid search results$")
		public void should_see_the_valid_search_results() throws Throwable {
			searchresultspage.verifySearchResultsPage(); 
		}	
 		
		}



	
	
	
	
		
	


