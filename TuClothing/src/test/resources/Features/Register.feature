Feature: Register


Scenario: Register functionality of Register button
Given user is in Tu home page 
When click on login &register link in header of the webpage
Then should show register buttton

Scenario:  registration functionality with Valid details
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on register button 
And user fill the registration form
Then it should show registration complete successfully

Scenario:  register a coustomer with Invalid email id
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on register button 
And user fill the registration form with invalid email id
Then it should show  enter valid emai id 

Scenario: Verify  register a customer without Title
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on register button 
And user fill the registration form without title
Then it should show error message 
