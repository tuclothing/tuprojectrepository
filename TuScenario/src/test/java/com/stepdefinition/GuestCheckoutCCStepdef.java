package com.stepdefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GuestCheckoutCCStepdef extends BaseClass {
	@Given("^i am in Home page$")
	public void i_am_in_Home_page() throws Throwable {
	  
	}
	@When("^enter valid email address$")
	public void enter_valid_email_address() throws Throwable {
		guestcheckoutccpage.verifyWithGuestEmail();
	}
	    
	

	@When("^click on guest checkout button$")
	public void click_on_guest_checkout_button() throws Throwable {
		guestcheckoutccpage.verifyGuestCheckoutButton();
	}

	@Then("^it should navigate Delivery option page$")
	public void it_should_navigate_Delivery_option_page() throws Throwable {
		guestcheckoutccpage.verifyDeliveryOptionPage();
	}
	@When("^I click on the radio button click &collect$")
	public void i_click_on_the_radio_button_click_collect() throws Throwable {
	    
		guestcheckoutccpage.verifyClickAndCollectionRadioButton();
	
	}
	@When("^click on continue button$")
	public void click_on_continue_button() throws Throwable {
	
		guestcheckoutccpage.verifyClickOnContinueButton();
		
		}
	

	@When("^I enter postcode$")
	public void i_enter_postcode() throws Throwable {
		guestcheckoutccpage.verifyLookUpPostcode();
	}

	@When("^click on look up button$")
	public void click_on_look_up_button() throws Throwable {
	   
		guestcheckoutccpage.verifyClickOnLookupButton();
	}
	@Then("^it should display all nearest shops according to the postcode$")
	public void it_should_display_all_nearest_shops_according_to_the_postcode() throws Throwable {
	   

	
	}


	
		
	}

		
