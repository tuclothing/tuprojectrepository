package com.stepdefinition;



import com.runner.BaseClass;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchResultsPageStepdef extends BaseClass{
	
	
	@When("^user selected one product from search results$")
	public void user_selected_one_product_from_search_results() throws Throwable {
					
		searchresultspage.verifyselectproductimage();	
       	
	}
	@Then("^it should show product details page$")
	public void it_should_show_product_details_page() throws Throwable {
		
		productDetailPage.verifyproductDetailsepage();
	}

}

	
