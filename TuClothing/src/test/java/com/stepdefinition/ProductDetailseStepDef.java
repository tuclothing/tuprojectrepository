package com.stepdefinition;


import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProductDetailseStepDef extends  BaseClass {
	@Given("^user is in product detailse page$")
	public void user_is_in_product_detailse_page() throws Throwable {
		productDetailPage.verifyproductDetailsepage();
	}

	@When("^user select the size & quantity\\.click on addtobasket button$")
	public void user_select_the_size_quantity_click_on_addtobasket_button() throws Throwable {	
		productDetailPage.verifyEnteringProductDetail();
		
	}

	@When("^click on add to basket$")
	public void click_on_add_to_basket() throws Throwable {	
		productDetailPage.verifyClickonaddtoBasket();
		
	}
	 @When("^click on basket link$")
		public void click_on_basket_link() throws Throwable {
		 productDetailPage.verifyClickonBasketlink();
	    	
	  	}

		@Then("^it should show the products in basket page$")
		public void it_should_show_the_products_in_basket_page() throws Throwable {
			basketdetailsepage.verifyBasketPage();
}
}
		