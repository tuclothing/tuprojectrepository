package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

public class BasketDetailsePage extends BaseClass {

	
	public static By QUANTITY = By.cssSelector("[name=quantity]");
	public static By QUANTITYUPDATEBUTTON = By.cssSelector("[href='#']");
	public static By PROCEEDCHECKOUT = By.cssSelector("#basketButtonTop");
	
	public void verifyBasketDetailsePage() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl());

	}	
	
	public void verifyUpdateSize() {
		WebDriverWait wait =new WebDriverWait(driver,10);		
	    wait.until(ExpectedConditions.elementToBeClickable( QUANTITY)).click(); 
	    driver.findElement(QUANTITY).sendKeys("3");
		
	}
	public void verifyClickQuantityUpdateButton() {
		//WebDriverWait wait =new WebDriverWait(driver,10); 
	//	wait.until(ExpectedConditions.elementToBeClickable(QUANTITYUPDATEBUTTON)).click(); 
		   
	//	wait.until(ExpectedConditions.elementToBeClickable (QUANTITYUPDATEBUTTON)).get(2).click();
	
		   driver.findElements(QUANTITYUPDATEBUTTON).get(2).click();
	}
	public void verifyClickProceedButton() {
		WebDriverWait wait =new WebDriverWait(driver,10);		
	    wait.until(ExpectedConditions.elementToBeClickable(PROCEEDCHECKOUT)).click(); 
		
	
	}	

	}

	
