Feature: Addbasket
Background:
Given i am in homepage



Scenario: Verify  Product Image on Product details page 
When I search for keyword "Jeans"
And I selected one product from search results page	
Then it should show product details page
 
Scenario: verify Add to basket
When I search for keyword "Jeans"
And I selected one product from search results page
When select the size "18R" & quantity.click on basket button
And click on basket link
Then it should show the products in basket page


Scenario: verify update basket
When I search for keyword "Jeans"
And I selected one product from search results page
When select the size "18R" & quantity.click on basket button
And click on basket link
Then it should show the products in basket page
When  update the Quntity "3"
And click on update button
Then it should update the quantity in basket

Scenario: verify Add to basket without select size & quantity
Given user is in home page
When user search for valid product name
And click on the search button
And user selected one product from search results 
And it should show product details page
And user is in product detailse page
And click on add to basket
Then user shouldn't add to basket
  