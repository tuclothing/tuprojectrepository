Feature: search



Scenario: :Verify the  functionality of search icon

Given user is in home page
When click on search icon
Then product search text box should display 

Scenario: Verify the functionality of search Box writing

Given user is in home page
When  user search for valid product name
Then it should type the valid product name in search box

Scenario: Verify search  with valid product name
Given user is in home page  
When user search for valid product name
And click on the search button
Then it should display all relevant products


Scenario: :Verify search  with valid product category

Given user  is in home page
When user search for valid product Department
Then Search result should display all the relevant products 

Scenario: :verify search with product name in special character

Given user is in home page
When user enter  product name with special character 
And click on search button
Then Search result should display relevant products

Scenario: Verify search multiple  products name comma separated

Given user is in home page
When enter multiple products with comma separated
Then it should  display products which one you entered first

Scenario: Verify search functionality of valid product number 

Given user in home page
When enter a valid product number
Then it should  display relevant product

Scenario: Verify search functionality of brand name

Given user is in home page  
When user search for a brand name Fatface
Then it should display relevent brand products

Scenario: verify search functionality with colour

Given user in home page
When enter a valid product with a perticular colour
Then it should display relevent products with the perticular colour

Scenario: verify search functionality with New arrivals

Given user in home page
When enter a valid product with New arrivals
Then it should display relevent products with New arrivals


Scenario: verify search functionality with invalid product

Given user in home page
When enter a invalid product name
Then it should show sorry message


Scenario: Verify search with invalid product number 

Given user in home page
When enter a invalid product number
Then it should  display sorry message







