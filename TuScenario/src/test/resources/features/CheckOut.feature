
Feature: CheckOut

Background:
Given i am in homepage

Scenario: Verify  Product Image on Product details page 
When I search for keyword "Jeans"
And I selected one product from search results page	
Then it should show product details page

Scenario: verify Add to basket
When I search for keyword "Jeans"
And I selected one product from search results page
When select the size "18R" & quantity.click on basket button
And click on basket link
Then it should show the products in basket page

Scenario: verify update basket
When I search for keyword "Jeans"
And I selected one product from search results page
When select the size "18R" & quantity.click on basket button
And click on basket link
When  update the Quntity "3"
And click on update button
Then it should update the quantity in basket

Scenario: verify proceed checkout button
When I search for keyword "Jeans"
And I selected one product from search results page
When select the size "18R" & quantity.click on basket button
And click on basket link
When  update the Quntity "3"
And click on update button
When I click on proceed button
Then it should navigate to checkout detailse page

Scenario: verify checkout with existing customer
When I search for keyword "Jeans"
And I selected one product from search results page
When select the size "18R" & quantity.click on basket button
And click on basket link
When  update the Quntity "3"
And click on update button
When I click on proceed button
When user enter valid email&password
And user click the login to checkout button
Then it wil navigate to payment detailse page 


