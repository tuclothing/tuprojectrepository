package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;
//import com.stepdefinition.product;
//import com.stepdefinition.search;
//import com.stepdefinition.with;



public class SearchResultsPage extends BaseClass {
	public void verifyHomepage() {
		Assert.assertEquals("Womens, Mens, Kids & Baby Fashion  | Tu clothing", driver.getTitle());	
	}

	
	public void verifySearchResultsPage() {
	
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=jeans", driver.getCurrentUrl());

	}
	
	public  void Verifysearchwithvalidproductcategory () {
	  Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/men/men?INITD=GNav-Men-Header", driver.getCurrentUrl());
	}
	public void verifysearchwithproductnameinspecialcharacter() {
	Assert.assertEquals("Search results for: $$$Shirts | Tu clothing", driver.getTitle());
	}
	public void Verifysearchmultipleproductsnamecommaseparated() {
		Assert.assertEquals("Search results for: Shirt,jeans | Tu clothing", driver.getTitle());
	}
	public void  Verifysearchfunctionalityofvalidproductnumber() {
		Assert.assertEquals("Search results for: 138138871 | Tu clothing",driver.getTitle());
	}
	public void  Verifysearchfunctionalityofbrandname() {
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=Fatface", driver.getCurrentUrl());
	}
	public void  verifysearchfunctionalitywithcolour() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=Black+Jeans", driver.getCurrentUrl());
	}
	public void  verifysearchfunctionalitywithNewarrivals() {
		Assert.assertEquals("Search results for: New arrivals | Tu clothing", driver.getTitle());
	}
	public void verifysearchfunctionalitywithinvalidproduct() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=vegitables",driver.getCurrentUrl());
	}
	public void Verifysearchwithinvalidproductnumber() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=00000",driver.getCurrentUrl()); 
	}	
	
	public void verifyselectproductimage() {		
	WebDriverWait wait =new WebDriverWait(driver,10);
	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='Mid Denim Slim Jean']"))).click();	
	

	}
	
	}
	
	
	