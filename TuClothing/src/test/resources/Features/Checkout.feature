Feature: checkout

Scenario:  Verify  Product Image on Product details page
Given user is in home page
When user search for valid product name
And user selected one product from search results 
Then it should show product details page

Scenario: verify Add to basket
Given user is in product detailse page
When user select the size & quantity.click on addtobasket button
And click on add to basket
And click on basket link
Then it should show the products in basket page

Scenario: verify update basket
Given user is in view basket page
When user update the size 
And click on update button
Then it should update the quantity in basket

Scenario: verify proceed checkout button
Given user is in view basket page
When user click on proceed button
Then it should navigate to checkout detailse page

Scenario: verify checkout with existing customer
Given user is in checkout detailse page
When user enter valid email&password
And user click the login to checkout button
Then it wil navigate to payment detailse page 


Scenario: verify checkout existing customer with invalid detailse
Given user is in checkout detailse page
When user enter invalid email&password
And user click the login to checkout button
Then it should show error message 