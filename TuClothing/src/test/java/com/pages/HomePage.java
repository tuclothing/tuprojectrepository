package com.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;
//import com.stepdefinition.product;
//import com.stepdefinition.search;
//import com.stepdefinition.with;





public class HomePage extends BaseClass {
	public static WebDriverWait wait;
	public static By Acceptcookies = By.cssSelector("#consent_prompt_submit");
	public static By SearchTextBox = By.cssSelector("#search");	
	public static By SearchButton  = By .cssSelector(".button.searchButton");
	public static By StoreLocaterIcon =By.cssSelector("[href='/store-finder']");
	
	public void verifyHomepage()  {

		System.setProperty("webdriver.chrome.driver", "C:\\Automation 2020\\chromedriver_win32\\chromedriver.exe" );  		
		driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();		

	}
		
	public void verifySearchProduct()	{
		WebDriverWait wait =new WebDriverWait(driver,10);		
	      wait.until(ExpectedConditions.elementToBeClickable(SearchTextBox)).clear(); 
	      driver.findElement(SearchTextBox).sendKeys("jeans");
	      driver.findElement(SearchButton).click();
	}

	public void verifysearchwithvalidproductcategory () {		
		 WebDriverWait  wait =new WebDriverWait(driver,10);
		 wait.until(ExpectedConditions.elementToBeClickable(SearchTextBox)).clear();	
		 driver.findElement(SearchTextBox).sendKeys("men");
		 driver.findElement(SearchButton).click();	
	
}
	public void verifysearchwithproductnameinspecialcharacter() {		
		driver.findElement(SearchTextBox).sendKeys("##$$$Shirts");	
		driver.findElement(SearchButton).click();
		
	}
	public void  Verifysearchmultipleproductsnamecommaseparated() {
		driver.findElement(SearchTextBox).sendKeys("Shirt,jeans");
		driver.findElement(SearchButton).click();	
	}	
	public void  Verifysearchfunctionalityofvalidproductnumber() {
		driver.findElement(SearchTextBox).sendKeys("138138871");		
		driver.findElement(SearchButton).click();
	}
	public void   Verifysearchfunctionalityofbrandname() {
		driver.findElement(SearchTextBox).sendKeys("Fatface");
		driver.findElement(SearchButton).click();
	}
	public void  verifysearchfunctionalitywithcolour() {
		driver.findElement(SearchTextBox).sendKeys("Black Jeans");
		driver.findElement(SearchButton).click();
		
	}
	public void verifysearchfunctionalitywithNewarrivals() {
		driver.findElement(SearchTextBox).sendKeys("New arrivals");
		driver.findElement(SearchButton).click();
	}
	public void verifysearchfunctionalitywithinvalidproduct() {
		driver.findElement(SearchTextBox).sendKeys("vegitables");
		driver.findElement(SearchButton).click();	
	}
	public void Verifysearchwithinvalidproductnumber() {
	driver.findElement(SearchTextBox).sendKeys("00000");		
	driver.findElement(SearchButton).click();
	}	    

	public void  VerifyStorelocatericon() {
	  WebDriverWait wait =new WebDriverWait(driver,10);
	  wait.until(ExpectedConditions.elementToBeClickable(StoreLocaterIcon)).click();
	   
	}

	
	
	}
	
	
		
		
