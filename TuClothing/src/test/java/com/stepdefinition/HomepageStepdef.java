package com.stepdefinition;



import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomepageStepdef extends BaseClass {

	
	@Given("^user  is in home page$")
	public void user_is_in_home_page1() throws Throwable {
		homepage.verifyHomepage();
		
	}	
	
	
	@Given("^user is in home page$")
	public void user_is_in_home_page() throws Throwable {
		homepage.verifyHomepage();
		}
	@When("^user search for valid product name$")
	public void user_search_for_valid_product_name() throws Throwable {
	   homepage.verifySearchProduct();
	}

	@Then("^it should type the valid product name in search box$")
	public void it_should_type_the_valid_product_name_in_search_box() throws Throwable {
		searchresultspage.verifySearchResultsPage();
	}
	
	@When("^click on search icon$")
	public void click_on_search_icon() throws Throwable {
	homepage.verifySearchProduct();
	
}
@When("^user search for valid product Department$")
	public void user_search_for_valid_product_Department() throws Throwable {
	homepage.verifysearchwithvalidproductcategory(); 
    
}

@Then("^Search result should display all the relevant products$")
public void search_result_should_display_all_the_relevant_products() throws Throwable {
	searchresultspage.Verifysearchwithvalidproductcategory(); 
}
@When("^user enter  product name with special character$")
public void user_enter_product_name_with_special_character() throws Throwable {
	homepage.verifysearchwithproductnameinspecialcharacter();
}
@When("^click on search button$")
public void click_on_search_button() throws Throwable {
	homepage.verifysearchwithproductnameinspecialcharacter();
}

@Then("^Search result should display relevant products$")
public void search_result_should_display_relevant_products() throws Throwable {
	searchresultspage.verifysearchwithproductnameinspecialcharacter();
}
@When("^enter multiple products with comma separated$")
public void enter_multiple_products_with_comma_separated() throws Throwable {
	homepage.Verifysearchmultipleproductsnamecommaseparated();
}
@Then("^it should  display products which one you entered first$")
public void it_should_display_products_which_one_you_entered_first() throws Throwable {
	searchresultspage.Verifysearchmultipleproductsnamecommaseparated();	
}
@Given("^user in home page$")
public void user_in_home_page() throws Throwable {
  homepage.verifyHomepage();
}
@When("^enter a valid product number$")
public void enter_a_valid_product_number() throws Throwable {
	homepage.Verifysearchfunctionalityofvalidproductnumber();
}
@Then("^it should  display relevant product$")
public void it_should_display_relevant_product() throws Throwable {
	searchresultspage.Verifysearchfunctionalityofvalidproductnumber();
}
@When("^user search for a brand name Fatface$")
public void user_search_for_a_brand_name_Fatface() throws Throwable {
	homepage.Verifysearchfunctionalityofbrandname();
}
@Then("^it should display relevent brand products$")
public void it_should_display_relevent_brand_products() throws Throwable {
	searchresultspage.Verifysearchfunctionalityofbrandname();
}
@When("^enter a valid product with a perticular colour$")
public void enter_a_valid_product_with_a_perticular_colour() throws Throwable {
	homepage.verifysearchfunctionalitywithcolour();
}

@Then("^it should display relevent products with the perticular colour$")
public void it_should_display_relevent_products_with_the_perticular_colour() throws Throwable {
	searchresultspage.verifysearchfunctionalitywithcolour();
}
@When("^enter a valid product with New arrivals$")
public void enter_a_valid_product_with_New_arrivals() throws Throwable {
	homepage.verifysearchfunctionalitywithNewarrivals();
	}

@Then("^it should display relevent products with New arrivals$")
public void it_should_display_relevent_products_with_New_arrivals() throws Throwable {
	searchresultspage.verifysearchfunctionalitywithNewarrivals();
}
@When("^enter a invalid product name$")
public void enter_a_invalid_product_name() throws Throwable {
	homepage.verifysearchfunctionalitywithinvalidproduct();
}

@Then("^it should show sorry message$")
public void it_should_show_sorry_message() throws Throwable {
	searchresultspage.verifysearchfunctionalitywithinvalidproduct();
}
@When("^enter a invalid product number$")
public void enter_a_invalid_product_number() throws Throwable {
	homepage.verifysearchfunctionalitywithinvalidproduct();
}
@Then("^it should  display sorry message$")
public void it_should_display_sorry_message() throws Throwable {
	searchresultspage.verifysearchfunctionalitywithinvalidproduct();
}	
@Given("^user is in Tu home page$")
public void user_is_in_Tu_home_page() throws Throwable {
	homepage.verifyHomepage();
}

@When("^user click on the Store locater icon on the top of the home page$")
public void user_click_on_the_Store_locater_icon_on_the_top_of_the_home_page() throws Throwable {
	homepage.VerifyStorelocatericon();
}

@Then("^it should show store locater page$")
public void it_should_show_store_locater_page() throws Throwable {
	storelocaterpage.verifystorelocaterpage();


}
@When("^type postcode on the search box$")
public void type_postcode_on_the_search_box() throws Throwable {
	storelocaterpage.Verifysearchstorelocaterpostcode();
}

@Then("^it should show all  stores nearby postcode$")
public void it_should_show_all_stores_nearby_postcode() throws Throwable {
	storelocaterpage.storelocaterpostcode();
}

}

	