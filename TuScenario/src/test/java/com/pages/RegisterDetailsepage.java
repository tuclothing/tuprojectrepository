package com.pages;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;


public class RegisterDetailsepage extends BaseClass {
	public static WebDriverWait wait;
	public static By RegisterBox = By.cssSelector(".ln-c-button.ln-c-button--primary.regToggle");
	public static By EmailAddress= By.cssSelector("#register_email");
	public static By SelectTitle = By.cssSelector("#register_title");
	public static By FirstName   =   By.cssSelector("#register_firstName");
	public static By surName     = By.cssSelector("[name=lastName]");
	public static By PassWord    =  By.cssSelector("#password");
	public static By ConfirmPassWord   =   By.cssSelector("#register_checkPwd");
	public static By  SubmitRegister   =   By.cssSelector("#submit-register");
	public void verifyRegister (Map<String,String> registerdetails) {
		 String email = registerdetails.get("Email");
		  String title = registerdetails.get("Title");
		  String firstname = registerdetails.get("Firstname");
		  String lastname = registerdetails.get("|Lastname");
		  String password = registerdetails.get("Password");
		  String confirmPassword = registerdetails.get("Confirmpassword");
		  
		  WebDriverWait wait =new WebDriverWait(driver,10);		
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ln-c-button.ln-c-button--primary.regToggle"))).click();
		 // driver.findElement(RegisterBox).click();
		  driver.findElement( EmailAddress).click();
			driver.findElement( EmailAddress).sendKeys(email);
			driver.findElement(SelectTitle).click();		
			driver.findElement(SelectTitle).sendKeys(title);
			driver.findElement(FirstName).click();
			driver.findElement(FirstName).sendKeys(firstname);
			driver.findElement(surName).click();
			driver.findElement(surName).sendKeys(lastname);
			driver.findElement(PassWord).click();
			driver.findElement(PassWord).sendKeys(password);
			driver.findElement(ConfirmPassWord ).click();
			driver.findElement(ConfirmPassWord ).sendKeys(confirmPassword);
			driver.findElement( SubmitRegister).click();	
	}
	
		
	}
	
	


