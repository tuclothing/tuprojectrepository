package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

public class BasketDetailsePage extends BaseClass {
	public static WebDriverWait wait;
	public static By QUANTITY = By.cssSelector("[name=quantity]");
	public static By QUANTITYUPDATEBUTTON = By.cssSelector("[href='#']");
	public static By PROCEEDCHECKOUT = By.cssSelector("#basketButtonTop");
	public  void verifyBasketPage() {
		  
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl());

	}
	
	public void verifyBasketqQuantityUpdate() {
		WebDriverWait wait =new WebDriverWait(driver,10);		
	    wait.until(ExpectedConditions.elementToBeClickable( QUANTITY)).click(); 
	    driver.findElement(QUANTITY).sendKeys("3");
	}
	public void verifyClickQuantityUpdateButton() {
		driver.findElements(QUANTITYUPDATEBUTTON).get(2).click();
		

	}
	public void verifyClickCheckotProceed() {
		driver.findElement(PROCEEDCHECKOUT).click();
	
	}
	
	}

	
	