package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class OrderSummaryPage extends  BaseClass{
	
	
	public void verifyHowToPayPage() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/order-summary/view",driver.getCurrentUrl());
	}
	public void verifyClickProceedToPayment() {
		driver.findElements(By.cssSelector("[data-testid='submit-button']")).get(0).click();
		
	}

		public void verifySummaryOrderPage() {
		 Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/payment-method/add",driver.getCurrentUrl()); 
	}
	}

