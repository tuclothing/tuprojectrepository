package com.runner;

import org.openqa.selenium.WebDriver;

import com.pages.BasketDetailsePage;
import com.pages.CheckoutDetailsePage;
import com.pages.GuestCheckoutCCPage;
import com.pages.HomeDetailsePage;
import com.pages.LoginDetailsePage;
import com.pages.ProductDetailsePage;
import com.pages.RegisterDetailsepage;
import com.pages.SearchResultsPage;

public class BaseClass {
	public static WebDriver driver;
	public static HomeDetailsePage homedetailsepage = new HomeDetailsePage() ;
	public static SearchResultsPage searchresultspage = new SearchResultsPage() ;
	public static ProductDetailsePage productdetailsepage = new ProductDetailsePage() ;
	public static BasketDetailsePage basketdetailsepage   = new BasketDetailsePage()  ;
	public static CheckoutDetailsePage checkoutdetailsepage= new CheckoutDetailsePage() ;
	public static GuestCheckoutCCPage  guestcheckoutccpage  = new  GuestCheckoutCCPage() ;
	public static  RegisterDetailsepage registerdetailsepage=new RegisterDetailsepage() ;
	public static  LoginDetailsePage   loginDetailsepage     = new  LoginDetailsePage() ;
}
