package com.stepdefinition;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AllStepDefinition {
	public static WebDriver driver;
	public static WebDriverWait wait;
	    
	

	
	

	
	    

	
	
	    
	/*	@When("^click on the search button$")
	public void click_on_the_search_button() throws Throwable {
		driver.findElement(By.cssSelector(".button.searchButton")).click();			
	    
	}

	@Then("^it should display all relevant products$")
	public void it_should_display_all_relevant_products() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=jeans", driver.getCurrentUrl());
	}
	@Given("^user  is in home page$")
	public void user_is_in_home_page1() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation 2020\\chromedriver_win32\\chromedriver.exe" );  		
		driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();		
	}*/
	    
	    
	
		
/*	@Then("^Search result should display all the relevant products$")
	public void search_result_should_display_all_the_relevant_products() throws Throwable {
	  Assert.assertEquals("https://tuclothing.sainsburys.co.uk/c/men/men?INITD=GNav-Men-Header", driver.getCurrentUrl());

	}*/
		
	 
	    
/*	@When("^user enter  product name with special character$")
	public void user_enter_product_name_with_special_character() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("##$$$Shirts");
		    
	}
	@When("^click on search button$")
	public void click_on_search_button() throws Throwable {
		driver.findElement(By.cssSelector(".button.searchButton")).click();
	   
	}

	@Then("^Search result should display relevant products$")
	public void search_result_should_display_relevant_products() throws Throwable {
		Assert.assertEquals("Search results for: $$$Shirts | Tu clothing", driver.getTitle());
	}*/
	/*@When("^enter multiple products with comma separated$")
	public void enter_multiple_products_with_comma_separated() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("Shirt,jeans");
		driver.findElement(By.cssSelector(".button.searchButton")).click();	
	   
	}
	@Then("^it should  display products which one you entered first$")
	public void it_should_display_products_which_one_you_entered_first() throws Throwable {
		Assert.assertEquals("Search results for: Shirt,jeans | Tu clothing", driver.getTitle());
	}
	@Given("^user in home page$")
	public void user_in_home_page1() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation 2020\\chromedriver_win32\\chromedriver.exe" );  		
		driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();
		
	}*/

	/*@When("^enter a valid product number$")
	public void enter_a_valid_product_number() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("138138871");		
		driver.findElement(By.cssSelector(".button.searchButton")).click();
	}

	@Then("^it should  display relevant product$")
	public void it_should_display_relevant_product() throws Throwable {
		Assert.assertEquals("Search results for: 138138871 | Tu clothing",driver.getTitle());
	}*/
	/*@When("^user search for a brand name Fatface$")
	public void user_search_for_a_brand_name_Fatface() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("Fatface");
		driver.findElement(By.cssSelector(".button.searchButton")).click();
	}

	@Then("^it should display relevent brand products$")
	public void it_should_display_relevent_brand_products() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=Fatface", driver.getCurrentUrl());
			
	}*/
	/*@When("^enter a valid product with a perticular colour$")
	public void enter_a_valid_product_with_a_perticular_colour() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("Black Jeans");
		driver.findElement(By.cssSelector(".button.searchButton")).click();
	}

	@Then("^it should display relevent products with the perticular colour$")
	public void it_should_display_relevent_products_with_the_perticular_colour() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=Black+Jeans", driver.getCurrentUrl()); 
	
	}*/	
	/*@When("^enter a valid product with New arrivals$")
	public void enter_a_valid_product_with_New_arrivals() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("New arrivals");
		driver.findElement(By.cssSelector(".button.searchButton")).click();
		}
	
	@Then("^it should display relevent products with New arrivals$")
	public void it_should_display_relevent_products_with_New_arrivals() throws Throwable {
		Assert.assertEquals("Search results for: New arrivals | Tu clothing", driver.getTitle());
	}*/
	
	
	/*@Given("^user is in Tu home page$")
	public void user_is_in_Tu_home_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation 2020\\chromedriver_win32\\chromedriver.exe" );  		
		driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();
		
	}
	    
	    
	@When("^user click on the Store locater icon on the top of the home page$")
	public void user_click_on_the_Store_locater_icon_on_the_top_of_the_home_page() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);
       	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[href='/store-finder']"))).click();
	   
	}

	@Then("^it should show store locater page$")
	public void it_should_show_store_locater_page() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());

	}	*/
	/*@When("^type postcode on the search box$")
	public void type_postcode_on_the_search_box() throws Throwable {
	 	driver.findElement(By.cssSelector("input[name='q']")).click();
		driver.findElement(By.cssSelector("input[name=q]")).sendKeys("Rg14 5Lf");
	   
	}

	@Then("^it should show all  stores nearby postcode$")
	public void it_should_show_all_stores_nearby_postcode() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}*/
	@When("^type location on the search box$")
	public void type_location_on_the_search_box() throws Throwable {
		driver.findElements(By.cssSelector("label.ln-c-form-option__label")).get(1).click();
	   
	}

	@Then("^it should show all  stores according the location$")
	public void it_should_show_all_stores_according_the_location() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}
	@When("^user type valid postcode on the search box$")
	public void user_type_valid_postcode_on_the_search_box() throws Throwable {
		driver.findElements(By.cssSelector(".ln-c-form-option__label")).get(3).click();
	   
	}

	@When("^click on click &collect$")
	public void click_on_click_collect() throws Throwable {
		driver.findElements(By.cssSelector(".ln-c-button.ln-c-button--primary")).get(2).click();
	}

	@Then("^it should show only click &collect shop near by postcode$")
	public void it_should_show_only_click_collect_shop_near_by_postcode() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());
	}
	@When("^user search without entering post code$")
	public void user_search_without_entering_post_code() throws Throwable {
		driver.findElement(By.cssSelector("input[name='q']")).click();
		driver.findElement(By.cssSelector("input[name=q]")).sendKeys("xxyy xxx");	   
	}

	@Then("^it should display error message$")
	public void it_should_display_error_message() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText());   
	}	
		
	@When("^user search with other country name \\(China\\)$")
	public void user_search_with_other_country_name_India() throws Throwable {
		driver.findElement(By.cssSelector("input[name='q']")).click();
		driver.findElement(By.cssSelector("input[name=q]")).sendKeys("china");	
	}	

	@Then("^it should display error message ? No store found$")
	public void it_should_display_error_message_No_store_found() throws Throwable {
		Assert.assertEquals("Store Locator", driver.findElement(By.cssSelector("h1")).getText()); 
	}
/*	@When("^click on login &register link in header of the webpage$")
	public void click_on_login_register_link_in_header_of_the_webpage() throws Throwable {
		driver.findElement(By.cssSelector("[href='/login']")).click();
				
	}*/

	@When("^click on login button$")
	public void click_on_login_button() throws Throwable {
		driver.findElement(By.cssSelector("[href='/login']")).click(); 
	}

	@Then("^it should display TU login detailse page$")
	public void it_should_display_TU_login_detailse_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login",driver.getCurrentUrl());
		
	}
	
		
	@When("^customer  fill the valid login details$")
	public void customer_fill_the_valid_login_details() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);
       	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#j_username"))).click();	
	   
	}

	@Then("^they should able to login successful$")
	public void they_should_able_to_login_successful() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login",driver.getCurrentUrl());
		 	
	}	
	@When("^customer  fill the Email login details$")
	public void customer_fill_the_Email_login_details() throws Throwable {
		driver.findElement(By.cssSelector("#j_username")).sendKeys("Sam@gmail.com");
		 
	}

	@Then("^they should  able to enter email successful$")
	public void they_should_able_to_enter_email_successful() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login",driver.getCurrentUrl());
	 
	}
	@When("^customer  fill the Password login details$")
	public void customer_fill_the_Password_login_details() throws Throwable {
		driver.findElement(By.cssSelector("#j_password")).click();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("abc1234");	
		driver.findElements(By.cssSelector(".ln-c-form-group")).get(2).click();
	}

	@Then("^they should  able to enter Password successful$")
	public void they_should_able_to_enter_Password_successful() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login",driver.getCurrentUrl());
	    
	}  
	@When("^customer  fill the invalid login details$")
	public void customer_fill_the_invalid_login_details() throws Throwable {
		driver.findElement(By.cssSelector("#j_username")).sendKeys("....000xmail");
		driver.findElement(By.cssSelector("#j_password")).click();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("xxxx");	
		driver.findElements(By.cssSelector(".ln-c-form-group")).get(2).click();
	}

	@Then("^they should not able to login successful$")
	public void they_should_not_able_to_login_successful() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login",driver.getCurrentUrl());
	    
	}
	
	@When("^customer login with valid email&wrong password$")
	public void customer_login_with_valid_email_wrong_password() throws Throwable {
		driver.findElement(By.cssSelector("#j_username")).sendKeys("Sam@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).click();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("xxxx");	
		driver.findElements(By.cssSelector(".ln-c-form-group")).get(2).click();  
	}
	@When("^customer login with blank email id$")
	public void customer_login_with_blank_email_id() throws Throwable {
		driver.findElement(By.cssSelector("#j_username")).sendKeys("");   
	}
/*	@When("^click on register button$")
	public void click_on_register_button() throws Throwable {
		driver.findElement(By.cssSelector("[href='/login']")).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.regToggle")).click();
		}
*/
	@Then("^should show register buttton$")
	public void should_show_register_buttton() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login",driver.getCurrentUrl());  
	}
/*	@When("^user fill the registration form$")
	public void user_fill_the_registration_form() throws Throwable {
		driver.findElement(By.cssSelector("#register_email")).click();
		driver.findElement(By.cssSelector("#register_email")).sendKeys("Sam@gmail.com");
		driver.findElement(By.cssSelector("#register_title")).click();		
		driver.findElement(By.cssSelector("#register_title")).sendKeys("MR");
		driver.findElement(By.cssSelector("#register_firstName")).click();
		driver.findElement(By.cssSelector("#register_firstName")).sendKeys("Sam");
		driver.findElement(By.cssSelector("[name=lastName]")).click();
		driver.findElement(By.cssSelector("[name=lastName]")).sendKeys("Tv");
		driver.findElement(By.cssSelector("#password")).click();
		driver.findElement(By.cssSelector("#password")).sendKeys("abc1234");
		driver.findElement(By.cssSelector("#register_checkPwd")).click();
		driver.findElement(By.cssSelector("#register_checkPwd")).sendKeys("abc1234");
		driver.findElement(By.cssSelector("#submit-register")).click();		
	}	
*/
	/*@Then("^it should show registration complete successfully$")
	public void it_should_show_registration_complete_successfully() throws Throwable {	   
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/register/success",driver.getCurrentUrl()); 
		
	
	}*/
	@When("^user fill the registration form with invalid email id$")
	public void user_fill_the_registration_form_with_invalid_email_id() throws Throwable {
		driver.findElement(By.cssSelector("#register_email")).click();
		driver.findElement(By.cssSelector("#register_email")).sendKeys("ss12//email");
	   
	}

	@Then("^it should show  enter valid emai id$")
	public void it_should_show_enter_valid_emai_id() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/register",driver.getCurrentUrl()); 
			
	}
	@When("^user fill the registration form without title$")
	public void user_fill_the_registration_form_without_title() throws Throwable {
		driver.findElement(By.cssSelector("#register_email")).click();
		driver.findElement(By.cssSelector("#register_email")).sendKeys("Sam@gmail.com");		
		driver.findElement(By.cssSelector("#register_firstName")).click();
		driver.findElement(By.cssSelector("#register_firstName")).sendKeys("Sam");
		driver.findElement(By.cssSelector("[name=lastName]")).click();
		driver.findElement(By.cssSelector("[name=lastName]")).sendKeys("Tv");
		driver.findElement(By.cssSelector("#password")).click();
		driver.findElement(By.cssSelector("#password")).sendKeys("abc1234");
		driver.findElement(By.cssSelector("#register_checkPwd")).click();
		driver.findElement(By.cssSelector("#register_checkPwd")).sendKeys("abc1234");
		driver.findElement(By.cssSelector("#submit-register")).click();		
		}

	@Then("^it should show error message$")
	public void it_should_show_error_message() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/register",driver.getCurrentUrl());
	}
	@Given("^User is in home page$")
	public void user_is_in_home_page11() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation 2020\\chromedriver_win32\\chromedriver.exe" );  		
		driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();		
     
	}
/*	@When("^user selected one product from search results$")
	public void user_selected_one_product_from_search_results() throws Throwable {
					
		WebDriverWait wait =new WebDriverWait(driver,10);
       	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt='Mid Denim Slim Jean']"))).click();	
       	
	}

	@Then("^it should show product details page$")
	public void it_should_show_product_details_page() throws Throwable {
		Assert.assertEquals("Womens Mid Denim Slim Jean | Tu clothing",driver.getTitle());
	}*/
/*	@Given("^user is in product detailse page$")
	public void user_is_in_product_detailse_page() throws Throwable {
		Assert.assertEquals("Womens Mid Denim Slim Jean | Tu clothing",driver.getTitle());
	}

	@When("^user select the size & quantity\\.click on addtobasket button$")
	public void user_select_the_size_quantity_click_on_addtobasket_button() throws Throwable {		
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#select-size"))).click();		
		driver.findElement(By.cssSelector("#select-size")).sendKeys("18R");		
		driver.findElement(By.cssSelector("#productVariantQty")).click();
	}

	@When("^click on add to basket$")
	public void click_on_add_to_basket() throws Throwable {	
		
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#AddToCart"))).click();		

	}


    @When("^click on basket link$")
	public void click_on_basket_link() throws Throwable {
		
    	driver.get("https://tuclothing.sainsburys.co.uk/");
		driver.findElements(By.cssSelector("[href='/cart']")).get(0).click();
		driver.findElements(By.cssSelector("[href='/cart']")).get(1).click();
  	}

	@Then("^it should show the products in basket page$")
	public void it_should_show_the_products_in_basket_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl());
	}*/
	/*@Given("^user is in view basket page$")
	public void user_is_in_view_basket_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl()); 
	}

	@When("^user update the size$")
	public void user_update_the_size() throws Throwable {
		driver.findElement(By.cssSelector("[name=quantity]")).click();
		driver.findElement(By.cssSelector("[name=quantity]")).sendKeys("3");
	}

	@When("^click on update button$")
	public void click_on_update_button() throws Throwable {
		driver.findElements(By.cssSelector("[href='#']")).get(2).click();
		
	}

	@Then("^it should update the quantity in basket$")
	public void it_should_update_the_quantity_in_basket() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/cart", driver.getCurrentUrl()); 
	   
	}*/

	/*@When("^user click on proceed button$")
	public void user_click_on_proceed_button() throws Throwable {
		driver.findElement(By.cssSelector("#basketButtonTop")).click();
	}

	@Then("^it should navigate to checkout detailse page$")
	public void it_should_navigate_to_checkout_detailse_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/checkout", driver.getCurrentUrl());
	} */
	/*@Given("^user is in checkout detailse page$")
	public void user_is_in_checkout_detailse_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login/checkout", driver.getCurrentUrl());   
	}

	@When("^user enter valid email&password$")
	public void user_enter_valid_email_password() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#j_username"))).click();				
		driver.findElement(By.cssSelector("#j_username")).sendKeys("sam@gmail.com");
		driver.findElement(By.cssSelector("#j_password")).click();
		driver.findElement(By.cssSelector("#j_password")).sendKeys("abc1234");
	}
	@When("^user click the login to checkout button$")
	public void user_click_the_login_to_checkout_button() throws Throwable {
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.js-login-button")).click();
	}

	@Then("^it wil navigate to payment detailse page$")
	public void it_wil_navigate_to_payment_detailse_page() throws Throwable {
		

	}*/
	/*
	@When("^user enter valid email address$")
	public void user_enter_valid_email_address() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#guest_email"))).click();			
		driver.findElement(By.cssSelector("#guest_email")).sendKeys("sss@gmail.com");		
	}*/

	/*@When("^user click on guest checkout button$")
	public void user_click_on_guest_checkout_button() throws Throwable {
		driver.findElements(By.cssSelector(".submit-container")).get(1).click(); 
		
	}

	@Then("^it should navigate Delivery option page$")
	public void it_should_navigate_Delivery_option_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/delivery-options/choose",driver.getCurrentUrl()); 
	
	}*/
	/*@Given("^user is in delivery option page$")
	public void user_is_in_delivery_option_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/delivery-options/choose",driver.getCurrentUrl()); 
	}
	@When("^user click on the radio button click &collect$")
	public void user_click_on_the_radio_button_click_collect() throws Throwable {
		driver.findElement(By.cssSelector("label[for='CLICK_AND_COLLECT']")).click();
	}

	@When("^user click on continue button$")
	public void user_click_on_continue_button() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ln-c-button.ln-c-button--primary"))).click();
	}

	@Then("^it should navigate find nearest click &collect store page$")
	public void it_should_navigate_find_nearest_click_collect_store_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/pickup-location/findStore",driver.getCurrentUrl()); 
		
	}*/
	/*@Given("^user is in find nearest click&collect store page$")
	public void user_is_in_find_nearest_click_collect_store_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/pickup-location/findStore",driver.getCurrentUrl());  
	}

	@When("^user enter postcode$")
	public void user_enter_postcode() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#lookup"))).click();		
		driver.findElement(By.cssSelector("#lookup")).sendKeys("Rg14 5lf");
	}

	@When("^user click on look up button$")
	public void user_click_on_look_up_button() throws Throwable {
		driver.findElement(By.cssSelector(".LocationLookup-module__inputButtons--29DUf")).click();
	}

	@Then("^it should display all nearest shops according to the postcode$")
	public void it_should_display_all_nearest_shops_according_to_the_postcode() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/pickup-location/findStore",driver.getCurrentUrl());  
	}
		
	
	@When("^user select one near shop$")
	public void user_select_one_near_shop() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".StoreCard-module__selectStoreButton--3CFno"))).click();	
		
	
	}

	@When("^user click on proceed to summary$")
	public void user_click_on_proceed_to_summary() throws Throwable {
		driver.findElement(By.cssSelector(".proceed-to-summary.ln-c-button.ln-c-button--primary")).click();
	}

	@Then("^it should navigate to Review your order and choose how to pay page$")
	public void it_should_navigate_to_Review_your_order_and_choose_how_to_pay_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/order-summary/view",driver.getCurrentUrl());
				
	}
	@Given("^user is in Review your order and choose how to pay page$")
	public void user_is_in_Review_your_order_and_choose_how_to_pay_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/order-summary/view",driver.getCurrentUrl()); 
	}

	@When("^user click on proceed to payment$")
	public void user_click_on_proceed_to_payment() throws Throwable {
		driver.findElements(By.cssSelector("[data-testid='submit-button']")).get(0).click();
	
	}

	@Then("^it should navigate to summary&order total page$")
	public void it_should_navigate_to_summary_order_total_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/payment-method/add",driver.getCurrentUrl()); 
	}
	@When("^user click on the radio button Home Delivery$")
	public void user_click_on_the_radio_button_Home_Delivery() throws Throwable {
		driver.findElements(By.cssSelector(".ln-c-form-option__label")).get(1).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary")).click();
	}	   
	

	@Then("^it should navigate to home delivery address page$")
	public void it_should_navigate_to_home_delivery_address_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/add",driver.getCurrentUrl());   
	}
	@Given("^user is in home delivery address page$")
	public void user_is_in_home_delivery_address_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/add",driver.getCurrentUrl());  
	}

	@When("^user select title from dropdown$")
	public void user_select_title_from_dropdown() throws Throwable {
		driver.findElement(By.cssSelector("[name='titleCode']")).click();
		driver.findElement(By.cssSelector("[name='titleCode']")).sendKeys("Ms");		
		
	}

	@When("^user enter the names&postcode$")
	public void user_enter_the_names_postcode() throws Throwable {
		driver.findElement(By.cssSelector("[name='firstName']")).click();
		driver.findElement(By.cssSelector("[name='firstName']")).sendKeys("Sam");
		driver.findElement(By.cssSelector("[name='lastName']")).click();
		driver.findElement(By.cssSelector("[name='lastName']")).sendKeys("Van");		
		driver.findElement(By.cssSelector("[name='postcode']")).click();
		driver.findElement(By.cssSelector("[name='postcode']")).sendKeys("Rg14 5lr");
	}

	@Then("^user should enter successful the detailse$")
	public void user_should_enter_successful_the_detailse() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/add",driver.getCurrentUrl()); 
	}
	
	@When("^user click on find address button$")
	public void user_click_on_find_address_button() throws Throwable {
		driver.findElement(By.cssSelector(".ln-c-button--primary.address-lookup.ln-u-push-bottom")).click();
	}
	@Then("^it should show select address dropdown& enter address manually$")
	public void it_should_show_select_address_dropdown_enter_address_manually() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/add",driver.getCurrentUrl());
	}	

	@When("^select a address from dropdpwn$")
	public void select_a_address_from_dropdpwn() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);	
	wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#addressResults"))).click();			
	driver.findElement(By.cssSelector("#addressListView.ln-c-select")).sendKeys("Flat 1 Trinity Court St Michaels Road"); 

	}

	@Then("^user can select address successful$")
	public void user_can_select_address_successful() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/add",driver.getCurrentUrl());  
	}
	@When("^user select use as billing address box$")
	public void user_select_use_as_billing_address_box() throws Throwable {
		driver.findElement(By.cssSelector("label[for='useDeliveryAddressForBillingAddress']")).click();
	}

	@When("^click on continue button$")
	public void click_on_continue_button() throws Throwable {
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[type='submit']"))).click();	
	}

	@Then("^it should navigate to optional delivery page$")
	public void it_should_navigate_to_optional_delivery_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/select-home-delivery-mode",driver.getCurrentUrl()); 
	}
	@Given("^user is in home delivery optional page$")
	public void user_is_in_home_delivery_optional_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/home-delivery/select-home-delivery-mode",driver.getCurrentUrl()); 
	}

	@When("^user click on Standard delivery radio button$")
	public void user_click_on_Standard_delivery_radio_button() throws Throwable {
	    driver.findElements(By.cssSelector("label[class='ln-c-form-option__label']")).get(0).click();
	 }

	@When("^click on continue$")
	public void click_on_continue() throws Throwable {
		driver.findElement(By.cssSelector("input[class='ln-c-button ln-c-button--primary']")).click();
	}

	@Then("^it should navigate to summary& payment page$")
	public void it_should_navigate_to_summary_payment_page() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/checkout/multi/order-summary/view",driver.getCurrentUrl()); 
		
	    	}
	
	/*@When("^enter a invalid product name$")
	public void enter_a_invalid_product_name() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("vegitables");
		driver.findElement(By.cssSelector(".button.searchButton")).click();	
	}

	@Then("^it should show sorry message$")
	public void it_should_show_sorry_message() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=vegitables",driver.getCurrentUrl());
		



	}	*/
/*	@When("^enter a invalid product number$")
	public void enter_a_invalid_product_number() throws Throwable {
		driver.findElement(By.cssSelector("#search")).sendKeys("00000");		
		driver.findElement(By.cssSelector(".button.searchButton")).click();
	  
	}

	@Then("^it should  display sorry message$")
	public void it_should_display_sorry_message() throws Throwable {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/search?text=00000",driver.getCurrentUrl()); 
	}*/

	}




