Feature: Guestcheckout

Scenario:  Verify  Product Image on Product details page
Given user is in home page
When user search for valid product name
And click on the search button
And user selected one product from search results 
Then it should show product details page

Scenario: verify Add to basket
Given user is in product detailse page
When user select the size & quantity.click on addtobasket button
And click on add to basket
And click on basket link
Then it should show the products in basket page

Scenario: verify update basket
Given user is in view basket page
When user update the size 
And click on update button
Then it should update the quantity in basket

Scenario: verify proceed checkout button
Given user is in view basket page
When user click on proceed button
Then it should navigate to checkout detailse page

Scenario: verify checkout with guest customer
Given user is in checkout detailse page
When user enter valid email address
And user click on guest checkout button
Then it should navigate Delivery option page

Scenario: verify checkout guest with Home Delivery
Given user is in delivery option page
When user click on the radio button Home Delivery
And user click on continue button
Then it should navigate to home delivery address page

Scenario: verify checkout guest with Home Delivery address
Given user is in home delivery address page
When user select title from dropdown
And user enter the names&postcode
Then user should enter successful the detailse

Scenario: verify checkout guest with Home Delivery Find address
Given user is in home delivery address page
When user click on find address button 
Then it should show select address dropdown& enter address manually

Scenario: verify checkout guest with Homedelivery findaddress with select address
Given user is in home delivery address page
When select a address from dropdpwn
Then user can select address successful

Scenario: verify checkout guest with Homedelivery with continue
Given user is in home delivery address page
When user select use as billing address box
And click on continue button
Then it should navigate to optional delivery page

Scenario: Verify home Delivery checkout with standard delivery
Given user is in home delivery optional page
 When user click on Standard delivery radio button
 And click on continue  
Then it should navigate to summary& payment page


