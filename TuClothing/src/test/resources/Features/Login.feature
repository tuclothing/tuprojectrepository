Feature: Login

Scenario: Login with valid details

Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on login button 
Then it should display TU login detailse page

Scenario: Login with valid details
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on login button
And customer  fill the valid login details
Then they should able to login successful

Scenario: Login with Email details
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on login button
And customer  fill the Email login details
Then they should  able to enter email successful

Scenario: Login with password details
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on login button
And customer  fill the Password login details
Then they should  able to enter Password successful

Scenario: Login with invalid details
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on login button
And customer  fill the invalid login details
Then they should not able to login successful

Scenario: Login with valid email & wrong password
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on login button
And customer login with valid email&wrong password
Then it should display error message

Scenario: Login with blank email id
Given user is in Tu home page 
When click on login &register link in header of the webpage 
And click on login button
And customer login with blank email id
Then it should display error message


