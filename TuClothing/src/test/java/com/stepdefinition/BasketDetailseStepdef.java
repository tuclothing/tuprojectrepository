package com.stepdefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BasketDetailseStepdef extends BaseClass {
	@Given("^user is in view basket page$")
	public void user_is_in_view_basket_page() throws Throwable {		
		 basketdetailsepage.verifyBasketPage();
	}

	@When("^user update the size$")
	public void user_update_the_size() throws Throwable {
		 basketdetailsepage.verifyBasketqQuantityUpdate();
		
	}

	@When("^click on update button$")
	public void click_on_update_button() throws Throwable {
		 basketdetailsepage.verifyClickQuantityUpdateButton();
		
	}

	@Then("^it should update the quantity in basket$")
	public void it_should_update_the_quantity_in_basket() throws Throwable {
		
}
	@When("^user click on proceed button$")
	public void user_click_on_proceed_button() throws Throwable {
		basketdetailsepage.verifyClickCheckotProceed();
		
		
	}

	@Then("^it should navigate to checkout detailse page$")
	public void it_should_navigate_to_checkout_detailse_page() throws Throwable {
		checkoutdetailsepage.verifyCheckoutPage();
		
	}
	}