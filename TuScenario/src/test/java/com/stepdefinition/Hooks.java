package com.stepdefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.runner.BaseClass;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends BaseClass {
	@Before
		public void start() {
		System.setProperty("webdriver.chrome.driver", "C:\\Automation 2020\\chromedriver_win32\\chromedriver.exe" );  		
		driver = new ChromeDriver();
		driver.get("https://tuclothing.sainsburys.co.uk/");	
		WebDriverWait wait =new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#consent_prompt_submit"))).click();	
	    
	}
		
	
	@After
		public void close() {
		driver.close() ;
	}
	}
