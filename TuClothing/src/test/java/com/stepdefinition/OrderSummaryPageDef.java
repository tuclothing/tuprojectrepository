package com.stepdefinition;


import com.runner.BaseClass;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class OrderSummaryPageDef extends BaseClass {
	

	@When("^user click on proceed to payment$")
	public void user_click_on_proceed_to_payment() throws Throwable {
		ordersummerypage.verifyClickProceedToPayment();
	
	}

	@Then("^it should navigate to summary&order total page$")
	public void it_should_navigate_to_summary_order_total_page() throws Throwable {
		ordersummerypage.verifySummaryOrderPage();
	}

}
