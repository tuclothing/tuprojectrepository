package com.stepdefinition;

import java.util.Map;

import org.junit.Assert;

import com.runner.BaseClass;


import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class RegisterdetailseStepdef extends BaseClass {

@Given("^I m in home page$")
public void i_m_in_home_page() throws Throwable {
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/",driver.getCurrentUrl());	
}

@When("^I enter Register detailse$")
public void i_enter_Register_detailse(DataTable registervalues) throws Throwable {
  Map<String,String> registerdetails = registervalues.asMap(String.class, String.class);
  registerdetailsepage.verifyRegister(registerdetails);
    
}


}
